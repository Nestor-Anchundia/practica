﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace leccion
{
    public class Program
    {
        static void Main(string[] args)
        {
            
            List<int> numeros = new List<int>() {  90, 87, 33, 1, 24, 24, 29, 27, 72, 59, 49, 87, 14, 92, 87, 95, 64,
                                         44, 94, 47, 24 ,95, 6, 47, 30, 13, 14 ,55 ,98, 15, 64 ,75, 0, 22,
                                         23, 77, 3 ,38, 99, 42, 39, 74, 29, 74, 15, 57, 39, 24, 51, 64 };
            Console.WriteLine();
            foreach (var registro in
               numeros.Select((v) => new { Valor = v }) // Obtener  valor
               .GroupBy(x => x.Valor) // Agrupar por el valor
                                      //.Where(x => x.ToList().Count() > 1) // En caso necesitas obtener cant. repetidas mayor a 1
               .Select(x => new {
                   Valor = x.Key, // key de la agrupación (valor)
                    Cantidad = x.Count(), // Cantidad de duplicidad
                }))
            {
                Console.WriteLine(string.Format("Valor: '{0}'\tCant. Repetidas: {1}\t", registro.Valor, registro.Cantidad));
            }
            Console.ReadKey();
        }
    }
}
